package com.beam.caseworkflowserver.config;

import com.beam.caseworkflowserver.model.CaseEvents;
import com.beam.caseworkflowserver.model.CaseStates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;
import org.springframework.statemachine.state.State;

import java.util.EnumSet;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig
        extends EnumStateMachineConfigurerAdapter<CaseStates, CaseEvents> {

    @Autowired
    private StateMachineRuntimePersister<CaseStates, CaseEvents, String> stateMachineRuntimePersister;

    @Override
    public void configure(StateMachineConfigurationConfigurer<CaseStates, CaseEvents> config)
            throws Exception {
        config
                .withPersistence()
                .runtimePersister(stateMachineRuntimePersister);
    }

    @Override
    public void configure(StateMachineStateConfigurer<CaseStates, CaseEvents> states)
            throws Exception {
        states
                .withStates()
                .initial(CaseStates.UNDER_CONSTRUCTION)
                .states(EnumSet.allOf(CaseStates.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<CaseStates, CaseEvents> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(CaseStates.UNDER_CONSTRUCTION)
                .target(CaseStates.NEW)
                .event(CaseEvents.QUEUE)
                .and()
                .withExternal()
                .source(CaseStates.NEW)
                .target(CaseStates.INVESTIGATION)
                .event(CaseEvents.ASSIGN)
                .and()
                .withExternal()
                .source(CaseStates.INVESTIGATION)
                .target(CaseStates.EXTENSION_REQUEST)
                .event(CaseEvents.EXTEND)
                .and()
                .withExternal()
                .source(CaseStates.INVESTIGATION)
                .target(CaseStates.REVIEW)
                .event(CaseEvents.SUBMIT)
                .and()
                .withExternal()
                .source(CaseStates.EXTENSION_REQUEST)
                .target(CaseStates.INVESTIGATION)
                .event(CaseEvents.APPROVE)
                .and()
                .withExternal()
                .source(CaseStates.EXTENSION_REQUEST)
                .target(CaseStates.INVESTIGATION)
                .event(CaseEvents.REJECT)
                .and()
                .withExternal()
                .source(CaseStates.REVIEW)
                .target(CaseStates.FILING_REVIEW)
                .event(CaseEvents.APPROVE)
                .and()
                .withExternal()
                .source(CaseStates.FILING_REVIEW)
                .target(CaseStates.CLOSED)
                .event(CaseEvents.CLOSE)
                .and()
                .withExternal()
                .source(CaseStates.FILING_REVIEW)
                .target(CaseStates.FILING)
                .event(CaseEvents.FILE)
                .and()
                .withExternal()
                .source(CaseStates.FILING)
                .target(CaseStates.FILING_FAILED)
                .event(CaseEvents.FAILED);
    }

    @Bean
    public StateMachineListener<CaseStates, CaseEvents> listener() {
        return new StateMachineListenerAdapter<CaseStates, CaseEvents>() {
            @Override
            public void stateChanged(final State<CaseStates, CaseEvents> from, final State<CaseStates, CaseEvents> to) {
                System.out.println("State change to " + to.getId());
            }
        };
    }
}
