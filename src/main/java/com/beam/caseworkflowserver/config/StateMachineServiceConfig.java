package com.beam.caseworkflowserver.config;

import com.beam.caseworkflowserver.model.CaseEvents;
import com.beam.caseworkflowserver.model.CaseStates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;
import org.springframework.statemachine.service.DefaultStateMachineService;
import org.springframework.statemachine.service.StateMachineService;

@Configuration
public class StateMachineServiceConfig {
    @Bean
    public StateMachineService<CaseStates, CaseEvents> stateMachineService(
            final StateMachineFactory<CaseStates, CaseEvents> stateMachineFactory,
            final StateMachineRuntimePersister<CaseStates, CaseEvents, String> stateMachineRuntimePersister) {
        return new DefaultStateMachineService<CaseStates, CaseEvents>(stateMachineFactory, stateMachineRuntimePersister);
    }
}
