package com.beam.caseworkflowserver.config;

import com.beam.caseworkflowserver.model.CaseEvents;
import com.beam.caseworkflowserver.model.CaseStates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.data.jpa.JpaPersistingStateMachineInterceptor;
import org.springframework.statemachine.data.jpa.JpaStateMachineRepository;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;

@Configuration
public class JpaPersisterConfig {
    @Bean
    public StateMachineRuntimePersister<CaseStates, CaseEvents, String> stateMachineRuntimePersister(
            final JpaStateMachineRepository jpaStateMachineRepository) {
        return new JpaPersistingStateMachineInterceptor<>(jpaStateMachineRepository);
    }
}
