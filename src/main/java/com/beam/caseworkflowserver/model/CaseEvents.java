package com.beam.caseworkflowserver.model;

public enum CaseEvents {
    QUEUE,
    ASSIGN,
    SUBMIT,
    APPROVE,
    REJECT,
    EXTEND,
    CLOSE,
    FILE,
    FAILED,
    REOPEN
}
