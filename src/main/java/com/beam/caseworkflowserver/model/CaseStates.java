package com.beam.caseworkflowserver.model;

public enum CaseStates {
    UNDER_CONSTRUCTION,
    NEW,
    INVESTIGATION,
    EXTENSION_REQUEST,
    REVIEW,
    FILING_REVIEW,
    FILING,
    FILING_FAILED,
    CLOSED
}
