package com.beam.caseworkflowserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaseWorkflowServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaseWorkflowServerApplication.class, args);
	}

}
