package com.beam.caseworkflowserver.service;

import com.beam.caseworkflowserver.model.CaseEvents;
import com.beam.caseworkflowserver.model.CaseStates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.service.StateMachineService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CaseWorkFlowService {
    @Autowired
    private StateMachinePersist<CaseStates, CaseEvents, String> stateMachinePersist;

    @Autowired
    private StateMachineService<CaseStates, CaseEvents> stateMachineService;

    @Autowired
    private StateMachineListener<CaseStates, CaseEvents> listener;

    public UUID createNewStateMachine() {
        final UUID uuid = UUID.randomUUID();
        final StateMachine<CaseStates, CaseEvents> caseStatesCaseEventsStateMachine = stateMachineService.acquireStateMachine(uuid.toString());

        caseStatesCaseEventsStateMachine.start();

        return uuid;
    }

    public CaseStates getNextState(final UUID uuid, final CaseEvents events){
        final StateMachine<CaseStates, CaseEvents> caseStatesCaseEventsStateMachine = stateMachineService.acquireStateMachine(uuid.toString());
        caseStatesCaseEventsStateMachine.sendEvent(events);

        final CaseStates state = caseStatesCaseEventsStateMachine.getState().getId();
        stateMachineService.releaseStateMachine(uuid.toString());

        return state;
    }
}
