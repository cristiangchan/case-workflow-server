package com.beam.caseworkflowserver.controller;

import com.beam.caseworkflowserver.model.CaseEvents;
import com.beam.caseworkflowserver.service.CaseWorkFlowService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "/work-flow")
public class CaseWorkFlowController {
    private final CaseWorkFlowService caseWorkFlowService;

    public CaseWorkFlowController(final CaseWorkFlowService caseWorkFlowService) {
        this.caseWorkFlowService = caseWorkFlowService;
    }

    @PostMapping
    public ResponseEntity<?> createStateMachine(){
        return ResponseEntity.status(HttpStatus.CREATED).body(caseWorkFlowService.createNewStateMachine());
    }

    @PostMapping("/{uuid}/event/{event}")
    public ResponseEntity<?> createStateMachine(@PathVariable final UUID uuid , @PathVariable final CaseEvents event) {
        return ResponseEntity.status(HttpStatus.CREATED).body(caseWorkFlowService.getNextState(uuid,event));
    }
}
